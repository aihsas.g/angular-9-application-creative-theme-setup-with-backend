import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from "@angular/forms";
import { appConfig } from '../../../app/validatons/validation';
import { AuthService } from '../auth.service';
import {Router} from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private loginService: AuthService,private router: Router,private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      employeeId: ['', [Validators.required,Validators.maxLength(appConfig.pattern.EMPLOYEEIDMAXLENGTH), Validators.minLength(appConfig.pattern.MINLENGTH), Validators.pattern(appConfig.pattern.NUMBER)]],
      password: ['', Validators.required]

    });
  }
  login() {
    this.spinner.show();
    this.loginService.userLogin(this.loginForm.value).subscribe((result: any) => {
      this.spinner.hide();
      if (result.code == 200) {
        this.toastr.success(result.message);
        this.router.navigateByUrl('/dashboard');
        let token = result.data
        let userId = result.data._id
        localStorage.setItem('Token', JSON.stringify(token));
        // localStorage.setItem('User ID', JSON.stringify(userId));

      } 
      else if(result.code ==501){
        // this.toastr.error(result.message);
      }
        else {
        // this.toastr.error(result.message);
      }

    })

  }

}
