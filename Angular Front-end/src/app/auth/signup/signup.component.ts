import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from "@angular/forms";
import { appConfig } from '../../../app/validatons/validation';
import { AuthService } from '../auth.service';
import {Router} from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private signupService: AuthService,private router: Router,private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.signupForm = this.formBuilder.group({
      employeeId: ['', [Validators.required,Validators.maxLength(appConfig.pattern.EMPLOYEEIDMAXLENGTH), Validators.minLength(appConfig.pattern.MINLENGTH), Validators.pattern(appConfig.pattern.NUMBER)]],
      password: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern(appConfig.pattern.EMAIL)]],
      username: ['', [Validators.required, Validators.maxLength(appConfig.pattern.MAXLENGTH), Validators.minLength(appConfig.pattern.MINLENGTH), Validators.pattern(appConfig.pattern.NAME)]]


    });
  }
  signUp() {
    this.spinner.show();
    this.signupService.userSignup(this.signupForm.value).subscribe((result: any) => {
      this.spinner.hide();
      if (result.code == 200) {
        this.toastr.success(result.message);
        this.router.navigateByUrl('/');
      } 
      else if(result.code ==500){
        this.toastr.error(result.message);
      }
      else {
        this.toastr.error(result.message);
      }
    })

  }

}
