import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { AppConst } from '../../app/utils/app_constants';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpService: HttpClient) { }

  userLogin(data: any): Observable<any> {
    return this.httpService.post(AppConst.USERLOGIN,data);
}

userSignup(data: any): Observable<any> {
  return this.httpService.post(AppConst.USERREGISTER,data);
}

getList(data: any): Observable<any> {
  return this.httpService.post(AppConst.USERLIST,data);
}

}
