export const appConfig = {
    pattern: {
        "NUMBER": /^[0-9]*$/,
        "EMAIL": /^(([^<>()\[\]\\.,,:\s@"]+(\.[^<>()\[\]\\.,,:\s@"-]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}-])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        "MAXLENGTH": 10,
        "EMPLOYEEIDMAXLENGTH": 20,
        "MINLENGTH": 4,
        "TIMEZONEMINLENGTH": 2,
        'NAME': /^[ A-Za-z0-9_@./#&+-/'/"]*$/,
       

       
    },
    paginator: {
        "COUNT": 2,
        "PAGE": 1
    },
   
};


