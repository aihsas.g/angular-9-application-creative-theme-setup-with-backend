import { Routes } from '@angular/router';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { IconsComponent } from '../../icons/icons.component';
import { AuthGuard } from '../../auth-gaurd/auth.guard';
import { UserListComponent } from 'app/user-list/user-list.component';
import { ProjectComponent } from 'app/project/project.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent,canActivate: [AuthGuard]},
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'user-list',     component: UserListComponent,canActivate: [AuthGuard]},
    { path: 'icons',          component: IconsComponent }, 
    { path: 'project',          component: ProjectComponent,canActivate: [AuthGuard]  }, 

];
