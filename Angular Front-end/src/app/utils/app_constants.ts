import { environment } from '../../environments/environment';
export class AppConst {

  private static appurl = environment.apiUrl;

  //user api
  public static get USERREGISTER(): string { return this.appurl + '/user/registerUser' };
  public static get USERLOGIN(): string { return this.appurl + '/user/userLogin' };
  public static get USERLIST(): string { return this.appurl + '/user/listUser' };
  public static get DELETEUSER(): string { return this.appurl + '/user/deleteUser' };
  public static get ADDPROJECT(): string { return this.appurl + '/project/addproject'};
  public static get LISTPROJECT(): string { return this.appurl + '/project/listproject'};
  public static get DELETEPROJECT(): string { return this.appurl + '/project/deleteproject'};
  public static get GETPROJECTBYID(): string { return this.appurl + '/project/getprojectbyid'};






};





