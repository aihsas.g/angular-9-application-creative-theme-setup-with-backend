import { Injectable } from '@angular/core';
import { CanActivate} from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}
  canActivate() {
      if (localStorage.getItem('Token')) {
        console.log("1")
          return true;
      }
      this.router.navigate(['/']);
      return false;
  }
  
}
