import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { AppConst } from '../../../app/utils/app_constants';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpService: HttpClient) { }
  getList(data: any): Observable<any> {
    return this.httpService.post(AppConst.USERLIST,data);
  }
  deleteUser(data: any): Observable<any> {
    return this.httpService.post(AppConst.DELETEUSER,data);
  }
}
