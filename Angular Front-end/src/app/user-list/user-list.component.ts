import { Component, OnInit } from '@angular/core';
import { UserService } from './service/user.service';
import { FormGroup,FormBuilder,FormControl} from "@angular/forms";
import { appConfig } from '../../app/validatons/validation';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  detailForList: any = {};
  userList: any = [];  
  searchUserForm: FormGroup;
  userTotalCount : any;
 userCountLink : any;
  usercount: number = appConfig.paginator.COUNT;
 userPage : number = appConfig.paginator.PAGE;


  constructor(private userService: UserService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,

    ) { }

  ngOnInit(): void {
        this.searchUserForm = this.formBuilder.group({
        searchText: new FormControl(''),
      });
    this.getUserListing();
  }
  getUserListing() {
    this.detailForList = {
      "count": this.usercount,
      "page": this.userPage,
      "searchText": this.searchUserForm.value.searchText ? this.searchUserForm.value.searchText : '',
      "userId":localStorage.getItem('User ID')

    }
    console.log("this.detailsFor list",this.detailForList)
    this.userService.getList(this.detailForList)
      .subscribe((res: any) => {
        if (res.code == 200) {
          this.userList = res.data ? res.data : this.userList;
          this.userTotalCount = res.totalCount;
          this.userCountLink = (this.userTotalCount / this.usercount);
          this.userCountLink = this.isFloats(this.userCountLink) ? Math.floor(this.userCountLink) + 1 : this.userCountLink;
          this.userCountLink = parseInt(this.userCountLink);
        }
        else{
          alert("something went wrong")
        }
      });
  }
  isFloats(n) {
    return Number(n) === n && n % 1 !== 0;
  }
  paginates(event) {
    this.userPage = event.page + 1;
    this.getUserListing();
  }

  deleteUser(data) {
    console.log("data",data)
    let tempObj = {
     userId: data._id ,
    }
    this.spinner.show();
     this.userService.deleteUser(tempObj).subscribe((result: any) => {
       console.log("result",result)
      this.spinner.hide();
        if (result && result.code == 200) {
        this.getUserListing();
        this.toastr.success(result.message);
      } 
       else if (result && result.code == 500) {
        this.toastr.error(result.message);
        
      }
    })
}
}

