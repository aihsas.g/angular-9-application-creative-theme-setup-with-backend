import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { AppConst } from '../../../app/utils/app_constants';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private httpService: HttpClient) { }

  addProject(data: any): Observable<any> {
    return this.httpService.post(AppConst.ADDPROJECT,data);
}

getProjectList(data: any): Observable<any> {
  return this.httpService.post(AppConst.LISTPROJECT,data);
}

deleteProject(data: any): Observable<any> {
  return this.httpService.post(AppConst.DELETEPROJECT,data);
}
getProjectByIddata(id: any): Observable<any> {
  console.log("data",id)
  return this.httpService.get(AppConst.GETPROJECTBYID,id);
}

}
