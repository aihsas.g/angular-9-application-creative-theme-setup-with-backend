import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl ,Validators} from "@angular/forms";
import { appConfig } from '../../app/validatons/validation';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';
import { ProjectService } from './service/project.service';
@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
  detailForProject: any = {};
  projectList: any = [];
  searchProjectForm: FormGroup;
  projectFormGroup: FormGroup;
  projectTotalCount: any;
  projectCountLink: any;
  displayModal: boolean = false;
  projectcount: number = appConfig.paginator.COUNT;
  projectPage: number = appConfig.paginator.PAGE;
  startDate: Date;
  minimumStartDate: Date;
  invalidDates: Array<Date>;
  setHeadMode: string = 'Create Project';
  btnMode: string = 'Create';
  projectData : any ;
  projectId:any;

  constructor(private projectService: ProjectService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService, ) { }

  ngOnInit(): void {
    this.searchProjectForm = this.formBuilder.group({
      searchText: new FormControl(''),
    });
    this.projectFormGroup = this.formBuilder.group({
      name: ['',[Validators.required,Validators.maxLength(appConfig.pattern.MAXLENGTH), Validators.minLength(appConfig.pattern.MINLENGTH), Validators.pattern(appConfig.pattern.NAME)]],
      consultant: ['',[Validators.required,Validators.maxLength(appConfig.pattern.MAXLENGTH), Validators.minLength(appConfig.pattern.MINLENGTH), Validators.pattern(appConfig.pattern.NAME)]],
      marketer: ['',[Validators.required,Validators.maxLength(appConfig.pattern.MAXLENGTH), Validators.minLength(appConfig.pattern.MINLENGTH), Validators.pattern(appConfig.pattern.NAME)]],
      startDate :['',Validators.compose([Validators.required])],
      technology :['',[Validators.required,Validators.maxLength(appConfig.pattern.MAXLENGTH), Validators.minLength(appConfig.pattern.MINLENGTH), Validators.pattern(appConfig.pattern.NAME)]],
      domain :['',[Validators.required,Validators.maxLength(appConfig.pattern.MAXLENGTH), Validators.minLength(appConfig.pattern.MINLENGTH), Validators.pattern(appConfig.pattern.NAME)]],
      timezone:['',[Validators.required,Validators.maxLength(appConfig.pattern.MAXLENGTH), Validators.minLength(appConfig.pattern.TIMEZONEMINLENGTH), Validators.pattern(appConfig.pattern.NAME)]]
    });
    this.getProjectListing();
  }
  getProjectListing() {
    this.detailForProject = {
      "count": this.projectcount,
      "page": this.projectPage,
      "searchText": this.searchProjectForm.value.searchText ? this.searchProjectForm.value.searchText : '',
      "userId":localStorage.getItem('User ID')

    }
    console.log("this.detailsFor list",this.detailForProject)
    this.projectService.getProjectList(this.detailForProject)
      .subscribe((res: any) => {
        if (res.code == 200) {
          this.projectList = res.data ? res.data : this.projectList;
          this.projectTotalCount = res.totalCount;
          this.projectCountLink = (this.projectTotalCount / this.projectcount);
          this.projectCountLink = this.isFloats(this.projectCountLink) ? Math.floor(this.projectCountLink) + 1 : this.projectCountLink;
          this.projectCountLink = parseInt(this.projectCountLink);
        }
        else{
          alert("something went wrong")
        }
      });
  }
  isFloats(n) {
    return Number(n) === n && n % 1 !== 0;
  }
  paginates(event) {
    this.projectPage = event.page + 1;
    this.getProjectListing();
  }

  deleteProject(data) {
    let tempObj = {
      projectId: data._id ,
    }
    this.spinner.show();
     this.projectService.deleteProject(tempObj).subscribe((result: any) => {
      this.spinner.hide();
        if (result && result.code == 200) {
        this.getProjectListing();
        this.toastr.success(result.message);
      } 
       else if (result && result.code == 500) {
        this.toastr.error(result.message);

      }
    })
  }
  getProjectById(id) {
    console.log("id",id)
    this.projectService.getProjectByIddata(id)
      .subscribe((res: any) => {
        if (res.code == 200) {
          console.log("pacth res",res)
          this.projectData = res.data
          //this.projectId = res.data._id;
          this.projectFormGroup.patchValue({
            name:res.data.name,
            consultant:res.data.consultant,
            marketer :res.data.marketer,
            startDate : res.data.startDate,
            technology : res.data.technology,
            domain : res.data.domain,
            timezone : res.data.timezone,
          });
        }
  });
}

  setMode(mode: string,id) {
   this.displayModal = true;
   if (mode == 'Edit') {
    this.btnMode = 'Update'
    this.setHeadMode = 'Update Subscription'
    this.getProjectById(id);
  }
  else {
  }

  }
  closeDialog() {
    this.projectFormGroup.reset();
    this.displayModal = false;
    this.invalidDates=[]

  }
  selectStartDate(){
    
  }
  restrictStartDate() {
    this.minimumStartDate = new Date();
  }

  createProject(){
    this.spinner.show();
    this.projectService.addProject(this.projectFormGroup.value).subscribe((result: any) => {
      this.spinner.hide();
      if (result.code == 200) {
        this.toastr.success(result.message);
        this.closeDialog();
        this.getProjectListing();
      } 
      else if(result.code ==501){
        this.toastr.error(result.message);
      }
        else {
        this.toastr.error(result.message);
      }

    })

  }


}