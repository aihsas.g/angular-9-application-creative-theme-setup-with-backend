import { Component, OnInit } from '@angular/core';
import { UserService } from 'app/user-list/service/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  detailForList: any = {};
  userListCount: any = [];  

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUserListing();
  }
  getUserListing() {
    this.detailForList = {
     
    }
    this.userService.getList(this.detailForList)
      .subscribe((res: any) => {
        if (res.code == 200) {
          console.log("res.fata",res)
          this.userListCount = res.totalCount ? res.totalCount : this.userListCount;

        }
        else{
          alert("something went wrong")
        }
      });
  }

}
