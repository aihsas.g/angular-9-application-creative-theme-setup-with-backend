import { CommonModule } from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { HttpClientModule } from '@angular/common/http'; 
import { AuthGuard } from '../app/auth-gaurd/auth.guard';
import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { SignupComponent } from './auth/signup/signup.component';
import { LoginComponent } from './auth/login/login.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { ToastrModule } from 'ngx-toastr';
import {CheckboxModule} from 'primeng/checkbox';
import {TableModule} from 'primeng/table';
import { UserListComponent } from './user-list/user-list.component';
import {PaginatorModule} from 'primeng/paginator';
import { ProjectComponent } from './project/project.component';
import {DialogModule} from 'primeng/dialog';
import {CalendarModule} from 'primeng/calendar';
import { TestComponent } from './auth/test/test.component';



@NgModule({
  imports: [
    CommonModule,
    TableModule,
    CalendarModule,
    DialogModule,
    BrowserAnimationsModule,
    FormsModule,
    PaginatorModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    CheckboxModule,
    NgxSpinnerModule,
    ToastrModule.forRoot({positionClass: 'toast-top-left'}) 
 
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    SignupComponent,
    LoginComponent,
    UserListComponent,
    ProjectComponent,
    TestComponent,

  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
